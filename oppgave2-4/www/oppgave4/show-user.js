import { LitElement, html } from '../node_modules/lit-element/lit-element.js';

class ShowUser extends LitElement {
  static get properties() {
    return {
      uid: {type: Number},
      hasAvatar: { type: Number},
      uname: { type: String },
      firstName: { type: String },
      lastName: { type: String }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <style>
      span {
        width: 14em;
        display: inline-block;
        overflow: hidden;
      }
      img {
        border-radius: 50px;
        width: 100px;
      }
      </style>
      <span>${this.uid}</span><span>${this.firstName} ${this.lastName}</span><span>${this.uname}</span>
    `;
  }
}

customElements.define('show-user', ShowUser);