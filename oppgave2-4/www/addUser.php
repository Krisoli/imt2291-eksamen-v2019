<?php

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

$res = [];
if (isset($_POST['submit']) // check if user has clicked on submit button
            &&!empty($_POST['username']) 
            &&!empty($_POST['password']) 
            &&!empty($_POST['firstname']) 
            &&!empty($_POST['lastname'])) {
           
            require_once 'classes/DB.php';
            $db = DB::getDBConnection();

            $stmt = $db->prepare('INSERT INTO user (uname, pwd, firstName, lastName) VALUES (?, ?, ?, ?)');
            $stmt->execute(array($_POST['username'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['firstname'], $_POST['lastname']));

            // If user added to db
            if($stmt->rowCount()==1) {
              $res['status'] = "Success!";
              $res['firstname'] = $_POST['firstname'];
              echo $twig->render('userAdded.html', $res);
            } else {
              $res['status'] = "Creation failed";
              echo $twig->render('addUser.html', $res);
            }
          } else {
            // render and display add user page 
          echo $twig->render('addUser.html', array());

          }
?>