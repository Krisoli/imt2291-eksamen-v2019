<?php
/**
 * Returns avatar image for given user id, send the user id as a GET parameter
 * with the name *id*.
 *
 * If no user with given id is found or that user has no avatar image nothing
 * is returned.
 */

header("Content-type: image/png");           // Avatar is always stored as png
require_once ('../classes/DB.php');
$db = DB::getDBConnection();

$sql = 'SELECT avatar FROM user WHERE uid=?';
$stmt = $db->prepare($sql);
$stmt->execute(array($_GET['id']));
if ($res=$stmt->fetch(PDO::FETCH_ASSOC)) {
  echo $res['avatar'];
}
