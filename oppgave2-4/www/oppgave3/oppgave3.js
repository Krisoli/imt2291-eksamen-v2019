



 function fetchUsers() {
    fetch('../api/fetchUsers.php', {
        method: 'Get',
        credentials: 'include'
    }).then(res => {
        return res.json()
    }).then(res => {
        console.log(res);
        const userUl = document.querySelector('.users ul');
        res.forEach( user => {
        const userList = document.createElement('LI');
        userList.setAttribute('data-uid', user.uid);
        userList.innerHTML = `${user.uid} ${user.firstName} ${user.lastName}`;
        userUl.appendChild(userList);
    })
    userUl.addEventListener('click', e=>{
        if (e.path[1].tagName=='LI') {
          editUser (e.path[1].dataset['uid']);
        } else if (e.target.tagName=='LI') {
          editUser (e.target.dataset['uid']);
        }
    })
 })
} 

fetchUsers();