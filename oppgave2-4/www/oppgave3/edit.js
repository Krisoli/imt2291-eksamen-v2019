window.uid = -1;
// Function for editing users from DB
function editUser(uid) {
    fetch(`../api/fetchUser.php?id=${uid}`)
    .then(res=>res.json())
    .then(user=>{
        window.uid = user.uid;
        document.getElementById('uname').value = user.uname;
        document.getElementById('firstName').value = user.firstName;
        document.getElementById('lastName').value = user.lastName;
    })

}

// Click on namelist triggers DB to new form and edit users. 

document.querySelector('input[type="submit"]').addEventListener('click', e=>{
    e.preventDefault();
    if(window.uid> -1) {
        const data = new FormData(e.target.form);
        data.append('uid', window.uid);
        fetch('../api/updateUser.php', {
            method: 'POST',
            body: data
        }).then(res=>res.json())
        .then(res=>{
            const response = document.querySelector('.response');
            if (res.status =='success') {
            response.innerHTML = "Informasjon oppdatert";
         } else {
            response.innerHTML = "Kunne ikke oppdatere informasjon om brukeren";
          }
        })
    }
 })

 // progress bar with avatar

 document.getElementById('avatar').addEventListener('change', e=>{
     if (window.uid==1)
     return;
    let file = e.target.files[0];
    const progress = document.querySelector('progress');
    progress.style.display = 'block';

    // CUT and Paste from javascript forelesning
    const xhr = new XMLHttpRequest();
    xhr.file = file; // not necessary if you create scopes like this
    xhr.addEventListener('progress', function(e) {
      const done = e.position || e.loaded, total = e.totalSize || e.total;
      progress.value = (Math.floor(done/total*1000)/10);
      progress.innerHTML = `${(Math.floor(done/total*100))}%`;
    }, false);
    if ( xhr.upload ) {
      xhr.upload.onprogress = function(e) {
        const done = e.position || e.loaded, total = e.totalSize || e.total;
        progress.value = (Math.floor(done/total*100));
        progress.innerHTML = `${(Math.floor(done/total*100))}%`;
      };
    }
    xhr.onreadystatechange = function(e) {
      if ( 4 == this.readyState ) {
        progress.style.display = 'none';
        const res = JSON.parse(e.target.response);
        console.log(['xhr upload complete', e, res]);
        fetchUsers();
        // File transfer successfull
      }
    };
    xhr.open('post', '/api/updateAvatar.php', true);
    xhr.setRequestHeader("Content-Type", "application/octet-stream");
    xhr.setRequestHeader("X-uid", window.uid);
    xhr.setRequestHeader("X-uname", document.getElementById('uname').value);
    xhr.send(file);
  })